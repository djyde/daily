# document

### 页面：

- 主页 index.html
- 搜索 search.html
- 搜索结果 result.html
- 文章 article.html
- 活动页 function.html

其中 layout.html 是 layout, 每个页面改变的是 `div#container` 的部分。

### 静态文件

编译好的静态文件都在 `/build` 目录中，需要用到的有：

- `global.js` 是唯一一个需要引入的 js 文件，在 layout 中引入
- 每个页面有对应名称的 css 文件
- 每个页面对应名称的 js 文件不需要引入
- `woff2`,`ttf`,`eot`,`svg`,`woff` 为 font-awesome 所需要的文件

**注意，请必须先给我提供静态文件的 path (public path/如cdn)，，因为有些文件的路径会写死在编译好的静态文件里**
