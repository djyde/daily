require('../styles/global.scss');
require('../bower_components/pure/grids-min.css');
require('../bower_components/normalize-css/normalize.css');
require('../styles/fontawesome/font-awesome.scss');


var FastClick = require('fastclick');
FastClick.attach(document.body);

var backBtn = document.querySelector('#back-btn');
var menuBtn = document.querySelector('#menu-btn');
var menu = document.querySelector('#menu');
var container= document.querySelector('#container');

backBtn.addEventListener('click', function(){
  menu.style.display = 'none';
})

menuBtn.addEventListener('click', function(){
  menu.style.display = 'inline';
  menuBtn.style.display = 'none';
  backBtn.style.display = 'inline';

  container.addEventListener('click', function(){
    menu.style.display = 'none';
    backBtn.style.display = 'none';
    menuBtn.style.display = 'inline';
  })
})

backBtn.addEventListener('click', function(){
  menu.style.display = 'none';
  backBtn.style.display = 'none';
  menuBtn.style.display = 'inline';
})
